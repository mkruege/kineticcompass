﻿# KineticCompass

This repository contains a clean implementation of the Kinetic Compass (KC) framework for experiment design/prioritization to optimize model (parameter) constraints. It can be applied to any process model or system and allows the implementation of additional modules that are specifically tailored to such.


## Publication

The publication explaining and testing the Kinetic Compass in detail will be linked here.  


## Authors and acknowledgment

Code: Matteo Krüger  
Method and research design: Thomas Berkemeier and Matteo Krüger  
Manuscript preparation and further discussion: Matteo Krüger, Ashmi Mishra, Peter Spichtinger, Ulrich Pöschl and Thomas Berkemeier  


## Installation

The KineticCompass package can be installed in Julia including all dependencies with the following command:

```
Pkg.add(url="https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass")
```


## Tutorials

I you want to apply the KineticCompass, we recommend using the tutorial that can be downloaded at https://gitlab.mpcdf.mpg.de/mkruege/kineticcompass-tutorials. It contains an example MATLAB model for the heterogeneous ozonolysis of oleic-acid along with a fit ensemble, a pre-calculated grid of model outputs and example scripts for the generation of such. The Jupyter Notebook is the actual tutorial, addressing the individual inputs and modules for the KineticCompass. You can adapt it to your specific system step-by-step.

In addition, you will find an advanced tutorial which addresses the implementation of modules like constraint potential metrics or optimization algroithms into the KC framework. This will allow you to specifically adapt the method for research questions associated with your own model and underlying system. If you wish to share your modules and add them to the KCModules library, or if you need help implementing a function, please don't hesitate to contact m.krueger@mpic.de.


## Documentation

The following documentation only contains a short description of the individual arguments and inputs for the KC framework. For more detailed information, please consider the tutorial and/or the manuscript that will be linked here after publication.

The KineticCompass is called with the following command:

```
using KineticCompass

KineticCompass.run_kinetic_compass(output_directory, fitfile, n_fits, constraint_potential_metric_name, constraint_potential_metric_func, gridspec, set_params, envorder; CP_optimizer_func, model_output_grid, output_model, separate_fitfile_columns, evaluate_full_input_grid, verbose)
```

### Positional Arguments

```
output_directory::AbstractString 
```
The relative path to the directory where the results are saved (from current pwd()) - will be created if not present.

```
fitfile::AbstractString
```
The path to a csv file containing the fit ensemble, representing the model's solution space. Rows are fits and columns are kinetic parameters. All columns must be either kinetic parameters, or specified as separate columns and required by the constraint potential metric.

```
n_fits::Integer
```
Number of fits from the fitfile that are used, starting from the top.

```
constraint_potential_metric_name::AbstractString
```
Name of the constraint potential metric that ís applied (required for plots and outputs).

```
constraint_potential_metric_func::Function
```
A Julia function that calculates constraint potentials from an ensemble of model outputs (ensemble solutions). This can be implemented by the user, or loaded from the KCModules module (currently available: Ensemble Spread, Parameter Constraint Potential (with or without error weighting), and Ensemble Label Distance, Target Constraint Potential). Use a lambda function to specify optional arguments for current run.

```
gridspec::Dict{String, Vector{Any}}
```
Specifications for the grid of environmental/experimental parameters that the KC is applied on. Specifies parameter names, lower and upper boundaries, number of grid points and if the grid is in linspace or logspace. Each parameter name (String) maps to a Vector in the following form:
```
[[lower_boundary::Float64, upper_boundary::Float64], grid_points::Int, logspace(0 or 1)::Int]
```

```
set_params::Dict{String, Float64}
```
Additional environmental/experimental parameters that are not varied (and thus not dimensions of the KC grid) can be specified here with a set value. If this is not required, pass an empty dictionary.

```
envorder::Vector{String}
```
The order of all environmental/experimental parameters (from gridspec AND set_params) as expected by the model or defining the data structure of the pre-evaluated grid of model outputs.

### Further 'optional' arguments

Note that some of these arguments are not fully optional. For instance, either a model or a pre-evaluated grid of model outputs MUST be provided. Same applies for evaluate_full_input_grid and CP_optimizer_func. It is generally recommended to use the optimizer with a model and evaluate the KC on the full input grid, if it is available. Default values of the arguments are shown here.

```
CP_optimizer_func::Union{Function, Nothing} = nothing
```
(Advised if you pass a slow model to the KC, and do not have a pre-evaluated grid of outputs.) A Julia function that contains an algorithm for constraint potential optimization on a discrete grid. The KCModules module provides a simple SGD-based discrete optimizer for this purpose. 

```
model_output_grid::Union{AbstractString, Nothing} = nothing
```
Path to a pre-evaluated grid of model outputs. This is currently implemented for MATLAB files, as well as .jls files. If you use the KC to read or evaluate a full grid, a .jls file will be created that can be passed as this argument for further KC calls. An example MATLAB script for the generation of such a pre-evaluated grid can be found among the KC tutorials.

```
output_model::Union{Function, Nothing} = nothing
```
(Required, if you don't have a pre-evaluated grid of model outputs.) A Julia function that calls a model to generate outputs for sets of kinetic + environmental parameters. You can use an engine to call models that are not based on the Julia programming language. An example for a MATLAB model is provided in the tutorial.

```
separate_fitfile_columns::Vector{String}=String[]
```
If the constraint potential metric requires additional information for each fit (e.g. labels or errors), you can add such column(s) to your fitfile and specifiy the column header here. 

```
evaluate_full_input_grid::Bool = true
```
Must be true if no optimization algorithm is specified. Will evaluate the KC on the full grid and enables plotting.

```
verbose::Bool = true
```
If set to false, unnecessary status updates are omitted.

### More options for plotting

After running the KC on the full grid, plots, csv data and a jls output file will be saved in the specified directory. If you want to add previous experiments to your plot, or apply other options like CP limits or colors, you can call the plotting functions with additional arguments and pass the path to the .jls file.

```
plot_contour(KC_output_file, output_directory; cp_lims, colmap, levels, prev_experiments) 
plot_barplot(KC_output_file, output_directory; barcolor, prev_experiments)
```

```
KC_output_file::AbstractString
```
Path to the result file saved as 'KC_evaluation_full_grid.jls'.

```
output_directory::AbstractString
```
Output directory for the advanced plots, will be created if not present.

```
cp_lims::Union{Tuple{Float64, Float64}, Nothing} = nothing
```
Manual lower and upper boundary for the constraint potential value in the contourplot.

```
colmap::Symbol = :turbo
```
Colormap scheme for the contourplot.

```
levels::Int = 20
```
Number of levels in the contourplot.

```
prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing
```
Previous experiments (varied parameters from gridspec).

```
barcolor::Symbol = :blue
```
Color of scatter points in barplot.


## Changelog

### 0.1.0
2023/08/01

First public version of the KineticCompass. 

### 1.1.0
2023/08/16

**Added**
- added normalization and standardization for parameter constraint potential to allow comparability between multiple kinetic parameters
- added plotting function to get violin plot for parameter constraint potentials of multiple kinetic parameters

**Changed**
- **grid boundaries always in linspace, even if grid is in logspace**
- rounded values in outputs
- y axis lower lim is set to 0 for barplots
- updated dependencies for Julia packages

### 1.2.0
2023/09/08

**Added**
- heatmap plot
- csv support for pre-evaluated grid of model outputs

**Changed**
- gridspec allows full grid axis as first argument (instead of boundaries) -> allows non-linear or -logarithmic grids

### 1.3.0
2023/09/19

**Added**
- csv output file for CP_matrix if system has two varied environmental parameters

**Changed**
- fixed issues that occured if envorder and order of parameters in gridspec differed
- fixed issues with uneven number of gridpoints for varied environmental parameters

### 1.4.0
2023/09/27

**Added**
- new metric 'target constraint potential': constraint potential is evaluated for a specific target condition or ensemble solution (can be selected from grid, evaluated by model or provided as separate_columns in fitfile)

**Changed**
- corrected names of CP_matrix files
- fixed error for model evaluation resulting from last update