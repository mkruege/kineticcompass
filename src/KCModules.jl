﻿# KINETIC COMPASS MODULES
# Constraint potential metrics, model wrappers and optimizers for the Kinetic Compass framework
# Matteo Krüger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/09/27


module KCModules

using Dierckx, Statistics, MATLAB, DataFrames, Random, CSV

global ORIGINAL_TARGET_ES = 0.0
const WARNED_ABOUT_COMPARABILITY = Ref(false)

function ensemble_spread(all_params_preds::DataFrame; # model outputs; always required!
    n_interp::Int = 0, # if outputs are interpolated, and to how many points
    x_vec::Vector{Float64} = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9], # if n_interp = true, please provide the x-vector for the model outputs (y)
    normalize::Bool = true,  # if the ensemble spread is normalized by the area under the ensemble mean
    nan_is_zero::Bool = true, # if NaNs in the model outputs are considered as 0 (if false, nans will be deleted before evaluation)
    delete_0rows::Bool = true # if all model outputs are 0, this solution is omitted for calculation
    )::Vector{Float64}
    
    if nan_is_zero # set nan solutions to 0
        matrix = Matrix(all_params_preds)
        matrix[isnan.(matrix)] .= 0
        all_params_preds = DataFrame(matrix, names(all_params_preds))
    else # delete nan solutions from ensemble
        rows_without_nan = .!any.(isnan, eachrow(all_params_preds))
        if any(rows_without_nan)
            all_params_preds = all_params_preds[rows_without_nan, :]
        else # if all rows contain NAN, return 0 for the ES
            return [0.0]
        end
    end

    if delete_0rows # find and delete rows with all zero values
        rows_with_all_zeros = [all(Vector(row) .== 0) for row in eachrow(all_params_preds)]
        all_params_preds = all_params_preds[.!rows_with_all_zeros, :]

        # If no rows remain, return 0 for the ES
        if nrow(all_params_preds) == 0
            return [0.0]
        end
    end

    if n_interp > 0
        # interpolate each row using second order spline interpolation
        interp_indices = LinRange(minimum(x_vec), maximum(x_vec), n_interp)
        interp_preds = DataFrame()
        for row in eachrow(all_params_preds)
            spline_interp = Spline1D(x_vec, Vector(row[1:end]); k=2)
            new_row = DataFrame([[spline_interp(interp_indices[i])] for i in 1:n_interp], ["o" * lpad(i, 3, '0') for i in 1:n_interp])
            interp_preds = vcat(interp_preds, new_row)
        end
    else
        interp_preds = all_params_preds
    end

    means = mean.(eachcol(interp_preds))
    stds = std.(eachcol(interp_preds))

    areas_mean = sum(means)
    areas_mean_std = sum(means + stds)
    areas_mean_minus_std = sum(means - stds)
    
    if normalize
        spread::Float64 = abs(areas_mean_std - areas_mean_minus_std) / abs(areas_mean)
    else
        spread = areas_mean_std - areas_mean_minus_std
    end

    if isnan(spread) # this will occur if every solution in the ensemble is 0 and normalization is activated (division by 0) -> ES should be 0
        spread = 0
    end
    
    return [spread]
end

function parameter_constraint_potential(all_params_preds::DataFrame, # model outputs
    all_params::DataFrame;  # model parameters for outputs (kinetic and experimental)
    error_metric::Function = mse_error_metric, # error metric to determine hypothetical fit acceptance
    error_th::Float64 = 1e-5,  # error threshold to determine hypothetical fit acceptance
    sample_density::Int = 1, # sample density, see Krüger et al. SI (recommended: (n_fits modulo sample_density = 1))
    nan_is_zero::Bool = true, # if NaNs in the model outputs are considered as 0 (if false, nans will be deleted before evaluation)
    delete_0rows::Bool = true, # if all model outputs are 0, this solution is omitted for calculation
    sortbyout::Union{Int, AbstractString} = "midval", # column that is used for initial separation into "negative and positive errors"; default: half-time; see Krüger et al. SI
    logparams::Bool = true, # if parameters are logarithmized by the function (must be false if they already are)
    logpreds::Bool = false, # if predictions are logarithmized by the function (consider error metric! Must be false if they already are)
    comparability::String = "standardize", # options to make constraint potentials comparable between multiple parameters: "none" nothing, "normalize" normalization of CP by difference between boundaries, "standardize" standardization of all param values
    weight_errors::Union{Nothing, Vector{Vector{Any}}} = nothing
    )::Vector{Float64}

    global WARNED_ABOUT_COMPARABILITY


    # check string input for comparability 
    if comparability !== "none"
        if comparability !== "standardize"
            if comparability !== "normalize"
                throw(ArgumentError("Invalid argument for comparability. Must be 'none', 'standardize' or 'normalize'."))
            end
        end
    end

    if nan_is_zero # set nan solutions to 0
        matrix = Matrix(all_params_preds)
        matrix[isnan.(matrix)] .= 0
        all_params_preds = DataFrame(matrix, names(all_params_preds))
    else # delete nan solutions from ensemble
        rows_without_nan = .!any.(isnan, eachrow(all_params_preds))
        if any(rows_without_nan)
            all_params_preds = all_params_preds[rows_without_nan, :]
        else # if all rows contain NAN, return 0 for the ES
            return [0.0 for x in 1:size(all_params_preds, 2)]
        end
    end

    if delete_0rows # find and delete rows with all zero values
        rows_with_all_zeros = [all(Vector(row) .== 0) for row in eachrow(all_params_preds)]
        all_params_preds = all_params_preds[.!rows_with_all_zeros, :]

        # If no rows remain, return 0 for the ES
        if nrow(all_params_preds) == 0
            return [0.0 for x in 1:size(all_params_preds, 2)]
        end
    end

    if logparams
        all_params = log10.(all_params)
    end

    if logpreds
        all_params_preds = log10.(all_params_preds)
    end

    # obtain solution average
    solution_average = mean(Matrix(all_params_preds), dims=1)[:]

    # sort all_params_preds and all_params based on the specified column
    if sortbyout == "midval"
        sortbyout_col = round(Int, size(all_params_preds, 2) / 2)
    elseif isa(sortbyout, AbstractString)
        sortbyout_col = findfirst(col -> col == sortbyout, names(all_params_preds))
    else
        sortbyout_col = sortbyout
    end

    sort_indices = sortperm(all_params_preds[!, sortbyout_col])
    sorted_all_params_preds = all_params_preds[sort_indices, :]
    sorted_all_params = all_params[sort_indices, :]
    if !isnothing(weight_errors)
        sorted_weights = [weight_errors[1][i] for i in sort_indices]
    end
    # split sorted DataFrame based on sortbyout average
    sortbyout_avg = mean(sorted_all_params_preds[!, sortbyout_col])
    sorted_greater = filter(row -> row[sortbyout_col] > sortbyout_avg, sorted_all_params_preds)
    sorted_smaller = filter(row -> row[sortbyout_col] <= sortbyout_avg, sorted_all_params_preds)

    # calculate error for each row in both DataFrames
    error_greater = []
    error_smaller = []
    for row in eachrow(sorted_greater)
        error = error_metric(Vector{Float64}(row), solution_average)
        push!(error_greater, error)
    end
    for row in eachrow(sorted_smaller)
        error = error_metric(Vector{Float64}(row), solution_average)
        push!(error_smaller, -error)
    end

    # append error to associated rows
    sorted_greater.error = error_greater
    sorted_smaller.error = -error_smaller

    # concatenate DataFrames and sort by new error
    sorted_all_params_preds = vcat(sorted_smaller, sorted_greater)
    sorted_indices = sortperm(sorted_all_params_preds, :error)
    sorted_all_params_preds = sorted_all_params_preds[sorted_indices, :]
    sorted_all_params = sorted_all_params[sorted_indices, :]

    # dict to save hypothetically accepted outputs
    matching_indices_dict = Dict{Int, Vector{Int}}()

    # apply sample_density, make sure that first and last element are present
    outer_indices::Vector{Int} = [1]
    while last(outer_indices) < size(sorted_all_params_preds, 1)
        curr = last(outer_indices) + sample_density
        if curr > size(sorted_all_params_preds, 1) #curr exceeds size of data
            if curr - size(sorted_all_params_preds, 1) > round(sample_density/2) # last element in list is too close, delete
                outer_indices[end] = size(sorted_all_params_preds, 1)
            else # last element in list is far enough, add
                push!(outer_indices, size(sorted_all_params_preds, 1))
            end
        else
            push!(outer_indices, curr)
        end
    end

    for outer_index in outer_indices # outer loop: solution assumed as truth
        outer_row = sorted_all_params_preds[outer_index, :]
        matching_indices::Vector{Int} = []
        for (inner_index, inner_row) in enumerate(eachrow(sorted_all_params_preds)) # inner loop: solutions that would remain in the ensemble
            error = error_metric(Vector{Float64}(outer_row[1:end-1]), Vector{Float64}(inner_row[1:end-1])) # error metric between the associated rows
            if error < error_th
                push!(matching_indices, inner_index)
            end
        end

        # save subset in dictionary
        matching_indices_dict[outer_index] = matching_indices
    end

    if comparability == "standardize"
        # standardize all parameter values so that CP metric is comparable despite different boundaries for different parameters
        for colname in names(sorted_all_params)
            currmean = mean(sorted_all_params[!, colname])
            currstd = std(sorted_all_params[!, colname])
            if currstd !== 0.0
                sorted_all_params[!, colname] = (sorted_all_params[!, colname] .- currmean) ./ currstd
            end
        end
    end

    # get min and max values for each parameter
    param_mins = minimum.(eachcol(sorted_all_params))
    param_maxs = maximum.(eachcol(sorted_all_params))

    # parameter constraint potential vector
    cp_vector = zeros(Float64, size(sorted_all_params, 2))

    # perform calculation for each subset-forming solution
    for (key, matching_indices) in pairs(matching_indices_dict)
        subset_params = sorted_all_params[matching_indices, :]
        
        for (param_index, param) in enumerate(eachcol(subset_params))
            if !isnothing(weight_errors)
                param_cp = (param_maxs[param_index] - quantile(param, 0.9) + quantile(param, 0.1) - param_mins[param_index])/(sorted_weights[key]/mean(sorted_weights)) #consider the weight (error of associated fit for simulated truth)
            else
                param_cp = (param_maxs[param_index] - quantile(param, 0.9) + quantile(param, 0.1) - param_mins[param_index])
            end
            cp_vector[param_index] += param_cp
        end
    end

    # normalize considering sample_density
    cp_vector /= length(matching_indices_dict)

    # normalize using initial parameter boundaries
    if comparability == "normalize"
        if logparams == false
            if !WARNED_ABOUT_COMPARABILITY[]
                println("WARNING: You have selected comparability by normalization and are not logarithmizing the parameters. If your parameters are already logarithmized, ignore this warning. Otherwise, results may be problematic/wrong.")
                WARNED_ABOUT_COMPARABILITY[] = true
            end
        end
        param_boundary_diff = param_maxs .- param_mins
        cp_vector = cp_vector ./ param_boundary_diff
    end

    return cp_vector

end

# overload function to handle extra error column for weighting
function parameter_constraint_potential(all_params_preds::DataFrame, # model outputs
    all_params::DataFrame,  # model parameters for outputs (kinetic and experimental)
    separate_columns::Vector{Vector{Any}}; # error values used to weight simulated truth assumption
    error_metric::Function = mse_error_metric, # error metric to determine hypothetical fit acceptance
    error_th::Float64 = 1e-5,  # error threshold to determine hypothetical fit acceptance
    sample_density::Int = 1, # sample density, see Krüger et al. SI (recommended: (n_fits modulo sample_density = 1))
    nan_is_zero::Bool = true, # if NaNs in the model outputs are considered as 0 (if false, nans will be deleted before evaluation)
    delete_0rows::Bool = true, # if all model outputs are 0, this solution is omitted for calculation
    sortbyout::Union{Int, AbstractString} = "midval", # column that is used for initial separation into "negative and positive errors"; default: half-time; see Krüger et al. SI
    logparams::Bool = true, # if parameters are logarithmized
    logpreds::Bool = false, # if predictions are logarithmized (consider error metric!)
    comparability::String = "none",
    weight_errors::Union{Nothing, Vector{Vector{Any}}} = nothing
    )::Vector{Float64}

    return parameter_constraint_potential(all_params_preds, all_params; error_metric=error_metric, error_th=error_th, sample_density=sample_density, nan_is_zero=nan_is_zero, delete_0rows=delete_0rows,
    sortbyout=sortbyout, logparams=logparams, logpreds=logpreds, comparability=comparability, weight_errors=separate_columns)

end

function ensemble_label_distance(all_params_preds::DataFrame, separate_columns::Vector{Vector{Any}}; logdis::Bool = false, normalize::Bool = false)::Vector{Float64}
    
    separate_column::Vector{Int} = separate_columns[1]

    # create a DataFrame with an additional column for labels
    labeled_preds = copy(all_params_preds)
    labeled_preds[!,:label] = separate_column

    # group the DataFrame by labels and exclude the 'label' column from the groups
    grouped_df = groupby(labeled_preds, :label)

    allmeans = []
    allstds = []

    # calculate the mean and standard deviation for each group (sub-DataFrame)
    for group in grouped_df
        mean_values = Matrix(combine(group, names(group, Not(:label)) .=> mean, renamecols=false))

        # convert mean_values to a Vector for easy calculations
        std_values = Matrix(combine(group, names(group, Not(:label)) .=> std, renamecols=false))

        push!(allmeans, mean_values)
        push!(allstds, std_values)
    end

    # initialize an array to store the distances
    distances = Float64[]

    for (group1idx, group1) in enumerate(allmeans)
        for (group2idx, group2) in enumerate(allmeans)
            if group1idx == group2idx
                continue
            end
            dists = Float64[]
            for (colidx, col) in enumerate(group1)
                if logdis # logarithmic distance
                    val = abs(log10(col) .- log10(group2[colidx]))
                else
                    val = abs(col .- group2[colidx])
                end
                if normalize # normalize by standard deviation
                    val = val/(allstds[group1idx][colidx] + allstds[group2idx][colidx])
                end
                push!(dists, val)
            end
            push!(distances, mean(dists))
        end
    end
    
    # calculate the final average distance
    avg_distance = mean(distances)

    return [avg_distance]
end

function target_constraint(all_params_preds::DataFrame, # model outputs
    separate_columns::Vector{Vector{Any}}; # outputs for selected target ensemble (will be generated automatically if not provided)
    error_metric::Function = logabs_error_metric, # error metric to determine hypothetical fit acceptance
    error_th::Float64 = 0.015, # error threshold to determine hypothetical fit acceptance
    n_interp::Int = 0, # if target ensemble solutions are interpolated, and to how many points
    normalize::Bool = true, # if the ensemble spread is normalized by the area under the ensemble mean
    sample_density::Int = 1, # sample density, see Krüger et al. SI (recommended: (n_fits modulo sample_density = 1))
    nan_is_zero::Bool = true, # if NaNs in the model outputs are considered as 0 (if false, nans will be deleted before evaluation)
    delete_0rows::Bool = true, # if all model outputs are 0, this solution is omitted for calculation
    sortbyout::Union{Int, AbstractString} = "midval", # column that is used for initial separation into "negative and positive errors"; default: half-time; see Krüger et al. SI
    logpreds::Bool = false)::Vector{Float64} # if predictions are logarithmized by the function (consider error metric! Must be false if they already are)
    
    # transform separate_columns into target_ensemble DataFrame
    col_names = Symbol.("out", 1:length(separate_columns))
    matrix_data = hcat(separate_columns...)
    target_ensemble = DataFrame(matrix_data, col_names)

    # subset-step: obtain subsets of hypothetically accepted fits from current ensemble solution
    if nan_is_zero # set nan solutions to 0
        matrix = Matrix(all_params_preds)
        matrix[isnan.(matrix)] .= 0
        all_params_preds = DataFrame(matrix, names(all_params_preds))
    else # delete nan solutions from ensemble
        rows_without_nan = .!any.(isnan, eachrow(all_params_preds))
        if any(rows_without_nan)
            all_params_preds = all_params_preds[rows_without_nan, :]
        else # if all rows contain NAN, return 0 for the ES
            return [0.0 for x in 1:size(all_params_preds, 2)]
        end
    end

    if delete_0rows # find and delete rows with all zero values
        rows_with_all_zeros = [all(Vector(row) .== 0) for row in eachrow(all_params_preds)]
        all_params_preds = all_params_preds[.!rows_with_all_zeros, :]

        # If no rows remain, return 0 for the ES
        if nrow(all_params_preds) == 0
            return [0.0 for x in 1:size(all_params_preds, 2)]
        end
    end

    if logpreds
        all_params_preds = log10.(all_params_preds)
    end

    # obtain solution average
    solution_average = mean(Matrix(all_params_preds), dims=1)[:]

    # sort all_params_preds and all_params based on the specified column
    if sortbyout == "midval"
        sortbyout_col = round(Int, size(all_params_preds, 2) / 2)
    elseif isa(sortbyout, AbstractString)
        sortbyout_col = findfirst(col -> col == sortbyout, names(all_params_preds))
    else
        sortbyout_col = sortbyout
    end

    sort_indices = sortperm(all_params_preds[!, sortbyout_col])
    sorted_all_params_preds = all_params_preds[sort_indices, :]

    # split sorted DataFrame based on sortbyout average
    sortbyout_avg = mean(sorted_all_params_preds[!, sortbyout_col])
    sorted_greater = filter(row -> row[sortbyout_col] > sortbyout_avg, sorted_all_params_preds)
    sorted_smaller = filter(row -> row[sortbyout_col] <= sortbyout_avg, sorted_all_params_preds)

    # calculate error for each row in both DataFrames
    error_greater = []
    error_smaller = []
    for row in eachrow(sorted_greater)
        error = error_metric(Vector{Float64}(row), solution_average)
        push!(error_greater, error)
    end
    for row in eachrow(sorted_smaller)
        error = error_metric(Vector{Float64}(row), solution_average)
        push!(error_smaller, -error)
    end

    # append error to associated rows
    sorted_greater.error = error_greater
    sorted_smaller.error = -error_smaller

    # concatenate DataFrames and sort by new error
    sorted_all_params_preds = vcat(sorted_smaller, sorted_greater)
    sorted_indices = sortperm(sorted_all_params_preds, :error)
    sorted_all_params_preds = sorted_all_params_preds[sorted_indices, :]

    # dict to save hypothetically accepted outputs
    all_matching_indices = []

    # apply sample_density, make sure that first and last element are present
    outer_indices::Vector{Int} = [1]
    while last(outer_indices) < size(sorted_all_params_preds, 1)
        curr = last(outer_indices) + sample_density
        if curr > size(sorted_all_params_preds, 1) #curr exceeds size of data
            if curr - size(sorted_all_params_preds, 1) > round(sample_density/2) # last element in list is too close, delete
                outer_indices[end] = size(sorted_all_params_preds, 1)
            else # last element in list is far enough, add
                push!(outer_indices, size(sorted_all_params_preds, 1))
            end
        else
            push!(outer_indices, curr)
        end
    end

    for outer_index in outer_indices # outer loop: solution assumed as truth
        outer_row = sorted_all_params_preds[outer_index, :]
        matching_indices::Vector{Int} = []
        for (inner_index, inner_row) in enumerate(eachrow(sorted_all_params_preds)) # inner loop: solutions that would remain in the ensemble
            error = error_metric(Vector{Float64}(outer_row[1:end-1]), Vector{Float64}(inner_row[1:end-1])) # error metric between the associated rows
            if error < error_th
                push!(matching_indices, inner_index)
            end
        end

        # save subset in dictionary
        push!(all_matching_indices, matching_indices)
    end

    
    # spread-step 1: obtain ensemble spread for target ensemble:
    global ORIGINAL_TARGET_ES
    if ORIGINAL_TARGET_ES == 0.0
        if nan_is_zero # set nan solutions to 0
            matrix = Matrix(target_ensemble)
            matrix[isnan.(matrix)] .= 0
            target_ensemble = DataFrame(matrix, names(target_ensemble))
        else # delete nan solutions from ensemble
            rows_without_nan = .!any.(isnan, eachrow(target_ensemble))
            if any(rows_without_nan)
                target_ensemble = target_ensemble[rows_without_nan, :]
            else # if all rows contain NAN, return 0 for the ES
                return [0.0]
            end
        end

        if delete_0rows # find and delete rows with all zero values
            rows_with_all_zeros = [all(Vector(row) .== 0) for row in eachrow(target_ensemble)]
            target_ensemble = target_ensemble[.!rows_with_all_zeros, :]

            # If no rows remain, return 0 for the ES
            if nrow(target_ensemble) == 0
                return [0.0]
            end
        end

        if n_interp > 0
            # interpolate each row using second order spline interpolation
            interp_indices = LinRange(minimum(x_vec), maximum(x_vec), n_interp)
            interp_preds = DataFrame()
            for row in eachrow(target_ensemble)
                spline_interp = Spline1D(x_vec, Vector(row[1:end]); k=2)
                new_row = DataFrame([[spline_interp(interp_indices[i])] for i in 1:n_interp], ["o" * lpad(i, 3, '0') for i in 1:n_interp])
                interp_preds = vcat(interp_preds, new_row)
            end
        else
            interp_preds = target_ensemble
        end

        means = mean.(eachcol(interp_preds))
        stds = std.(eachcol(interp_preds))

        areas_mean = sum(means)
        areas_mean_std = sum(means + stds)
        areas_mean_minus_std = sum(means - stds)
        
        if normalize
            target_spread::Float64 = abs(areas_mean_std - areas_mean_minus_std) / abs(areas_mean)
        else
            target_spread = areas_mean_std - areas_mean_minus_std
        end

        if isnan(target_spread) # this will occur if every solution in the ensemble is 0 and normalization is activated (division by 0) -> ES should be 0
            target_spread = 0.0
        end
        ORIGINAL_TARGET_ES = target_spread
    else
        target_spread = ORIGINAL_TARGET_ES
    end

    # spread-step 2: obtain spreads for hypothetically accepted fits in target ensemble
    sub_spreads::Vector{Float64} = []

    for (mi, matching_indices) in enumerate(all_matching_indices)

        target_sub_ensemble = DataFrame(target_ensemble[matching_indices, :])

        if nan_is_zero # set nan solutions to 0
            matrix = Matrix(target_sub_ensemble)
            matrix[isnan.(matrix)] .= 0.0
            target_sub_ensemble = DataFrame(matrix, names(target_sub_ensemble))
        else # delete nan solutions from ensemble
            rows_without_nan = .!any.(isnan, eachrow(target_sub_ensemble))
            if any(rows_without_nan)
                target_sub_ensemble = target_sub_ensemble[rows_without_nan, :]
            else # if all rows contain NAN, return 0 for the ES
                return [0.0]
            end
        end
    
        if delete_0rows # find and delete rows with all zero values
            rows_with_all_zeros = [all(Vector(row) .== 0.0) for row in eachrow(target_sub_ensemble)]
            target_sub_ensemble = target_sub_ensemble[.!rows_with_all_zeros, :]
    
            # If no rows remain, return 0 for the ES
            if nrow(target_sub_ensemble) == 0.0
                return [0.0]
            end
        end
    
        if n_interp > 0
            # interpolate each row using second order spline interpolation
            interp_indices = LinRange(minimum(x_vec), maximum(x_vec), n_interp)
            interp_preds = DataFrame()
            for row in eachrow(target_sub_ensemble)
                spline_interp = Spline1D(x_vec, Vector(row[1:end]); k=2)
                new_row = DataFrame([[spline_interp(interp_indices[i])] for i in 1:n_interp], ["o" * lpad(i, 3, '0') for i in 1:n_interp])
                interp_preds = vcat(interp_preds, new_row)
            end
        else
            interp_preds = target_sub_ensemble
        end
    
        means = mean.(eachcol(interp_preds))
        stds = std.(eachcol(interp_preds))
    
        areas_mean = sum(means)
        areas_mean_std = sum(means + stds)
        areas_mean_minus_std = sum(means - stds)
        
        if normalize
            spread::Float64 = abs(areas_mean_std - areas_mean_minus_std) / abs(areas_mean)
        else
            spread = areas_mean_std - areas_mean_minus_std
        end
    
        if isnan(spread) # this will occur if every solution in the ensemble is 0 and normalization is activated (division by 0) -> ES should be 0
            spread = 0.0
        end
        if spread != 0.0
            push!(sub_spreads, (target_spread/spread))
        end
    end
    return [mean(sub_spreads)]
end

function mse_error_metric(x::Vector{Float64}, y::Vector{Float64})
    error = sum((x .- y).^2) / length(x)
    return error
end

function logabs_error_metric(x::Vector{Float64}, y::Vector{Float64})

    N = []
    for i in 1:length(x)
        push!(N, (log10(x[i]) - log10(y[i]))^2)
    end
    rRMSE = mean(N)
    
    return rRMSE
end

function call_MATLAB_model_soot(all_params::Vector{DataFrame})::Vector{Vector{Vector{Float64}}}

    all_params_preds = []

    # loop over each DataFrame in all_params
    for (i, params_df) in enumerate(all_params)
        if length(all_params) > 1
            println("Obtaining ensemble solution $i of $(length(all_params))")
        end
        params_preds = []

        # loop over each lambda
        for (j, lambda_row) in enumerate(eachrow(params_df))

            lambda = Vector(lambda_row)

            try
                mat"""cd 20230619_Compass"""
            catch
            end

            # construct the MATLAB expression using the mat"" literal
            mat"""
                try
                    OUT=KM3_POET('MESSERER_5_fit3_kmonly',1,$lambda,[],7,2);
                    $result=OUT(:,2)';
                catch
                    disp("Encountered reaction that didn't terminate: setting outputs to 0")
                    $result=zeros(1,99);
                end
                cd ..    
            """
           push!(params_preds, vec(result))
        end
        push!(all_params_preds, params_preds)
    end
    return all_params_preds
end

function call_MATLAB_model_oleic(all_params::Vector{DataFrame})::Vector{Vector{Vector{Float64}}}

    all_params_preds = []

    # loop over each DataFrame in all_params
    for (i, params_df) in enumerate(all_params)
        if length(all_params) > 1
            println("Obtaining ensemble solution $i of $(length(all_params))")
        end
        params_preds = []

        # loop over each lambda
        for (j, lambda_row) in enumerate(eachrow(params_df))

            lambda = Vector(lambda_row)

            try
                mat"""cd KM3_model_OLO3_surrogate_model_study_20220603"""
            catch
            end

            # construct the MATLAB expression using the mat"" literal
            mat"""
                [X,Y]=KM3_POET('OLEICSIMPLE_ETH_sample',1,transpose($lambda),[],7,2);
                omit=[2, 4, 6, 8, 10, 12, 14, 16, 18];
                $result=X(1,omit);
                cd ..    
            """
           push!(params_preds, vec(result))
        end
        push!(all_params_preds, params_preds)
    end
    return all_params_preds
end

function discrete_optimizer_sgd(output_model::Function, # first argument is either predictive model function, or full grid of model outputs
    constraint_potential_metric_func::Function, # constraint potential metric function
    output_directory::AbstractString, # output_directory
    all_params_dict::Union{Dict{Tuple{Vararg{Int}}, DataFrame}}, # model input grid
    separate_columns::Union{Nothing, Vector{Vector{Any}}}, # additional separate columns if required by constraint potential metric function 
    envorder::Vector{String}; # list of experimental parameter names to get values from all_params_dict
    num_iterations::Int = 20, # number of iterations per optimization
    learning_rate::Int = 10, # maximum step size in each dimension
    decay::Int = 2, # number of iterations before learning_rate is decreased by 1
    tolerance::Float64 = 0.0, # tolerance to quit if no sufficient progress
    i_CP::Int = 1) # index of constraint potential metric function output that is optimized (always 1 for ensemble spread, kinetic_param_index for parameter CP)


    # randomly initialize the key for starting optimization
    current_key::Tuple{Vararg{Int}} = rand(keys(all_params_dict))

    # initialize the best key and metric value
    best_key::Tuple{Vararg{Int}} = current_key
    best_metric::Float64 = -Inf

    # initialize the previous metric value for convergence check
    prev_metric::Float64 = -Inf

    indices_columns = [Symbol(env) => Float64[] for env in envorder]
    all_results_df = DataFrame((indices_columns..., cp_value = Float64[]))
    successful_steps_df = DataFrame((indices_columns..., cp_value = Float64[]))

    tested_keys::Vector{Tuple{Vararg{Int}}} = []
    metric_requirements::Int64 = 0

    # perform optimization
    for iteration in 1:num_iterations
        if iteration % 100 == 0
            println("Iteration $iteration of $num_iterations")
            println("Best CP: $best_metric")
        end

        # call specified predictive model to obtain current ensemble solution
        current_params_preds = output_model([all_params_dict[current_key]])

        column_names = Symbol.("out", 1:size(current_params_preds[1][1], 1))
        current_params_preds_df = DataFrame()
        for i in 1:length(current_params_preds[1])
            sub_df = DataFrame(transpose(current_params_preds[1][i]), column_names)
            current_params_preds_df = vcat(current_params_preds_df, sub_df)
        end
        # evaluate the metric for the current key
        metric::Float64 = 0.0
        if metric_requirements == 0 # determine once which data is required by CP function
            try 
                if separate_columns == nothing
                    metric = constraint_potential_metric_func(current_params_preds_df)[i_CP]
                    metric_requirements = 1
                else
                    metric = constraint_potential_metric_func(current_params_preds_df, separate_columns)[i_CP]
                    metric_requirements = 2
                end
            catch
                if separate_columns == nothing
                    metric = constraint_potential_metric_func(current_params_preds_df, all_params_dict[current_key])[i_CP]
                    metric_requirements = 3
                else
                    metric = constraint_potential_metric_func(current_params_preds_df, all_params_dict[current_key], separate_columns)[i_CP]
                    metric_requirements = 4
                end
            end
        else # in further iterations, omit test
            if metric_requirements == 1
                metric = constraint_potential_metric_func(current_params_preds_df)[i_CP]
            elseif metric_requirements == 2
                metric = constraint_potential_metric_func(current_params_preds_df, separate_columns)[i_CP]
            elseif metric_requirements == 3
                metric = constraint_potential_metric_func(current_params_preds_df, all_params_dict[current_key])[i_CP]
            else
                metric = constraint_potential_metric_func(current_params_preds_df, all_params_dict[current_key], separate_columns)[i_CP]
            end
        end

        # update the best key and metric if necessary
        if metric > best_metric
            best_key = current_key
            best_metric = metric
            push!(successful_steps_df, ([all_params_dict[current_key][!,env][1] for env in envorder]..., metric))
            #for indices: push!(successful_steps_df, (collect(current_key)..., metric))
        end

        # update the learning rate based on the iteration
        if (iteration % decay == 0) && (learning_rate > 1)
            learning_rate -= 1  # Decay the learning rate
        end

        # check for convergence based on tolerance
        if abs(metric - prev_metric) < tolerance
            break
        end

        # update the previous metric for convergence check
        prev_metric = metric
        push!(all_results_df, ([all_params_dict[current_key][!,env][1] for env in envorder]..., metric))

        # update the random key by selecting a neighboring key randomly
        stopat = 1000
        while stopat != 0
            new_key = select_random_neighbor(best_key, learning_rate)
            stopat = stopat-1
            if stopat == 0
                println("Stopped optimization because no untested key was encountered.")
                CSV.write(joinpath(output_directory,"optimization_results_allsteps.csv"), all_results_df)
                CSV.write(joinpath(output_directory,"optimization_results_successful_steps.csv"), successful_steps_df)
                return
            end
            if collect(new_key) ∈ tested_keys
                continue
            else
                try
                    all_params_dict[new_key]
                    current_key = new_key
                    push!(tested_keys, current_key)
                    break
                catch
                    continue
                end
            end
        end

    end

    CSV.write(joinpath(output_directory,"optimization_results_allsteps.csv"), all_results_df)
    CSV.write(joinpath(output_directory,"optimization_results_successful_steps.csv"), successful_steps_df)

    return
end

function discrete_optimizer_sgd(all_params_preds_dict::Dict{Tuple{Vararg{Int}}, DataFrame},
    constraint_potential_metric_func::Function,
    output_directory::AbstractString,
    all_params_dict::Dict{Tuple{Vararg{Int}}, DataFrame},
    separate_columns::Union{Nothing, Vector{Vector{Any}}},
    envorder::Vector{String};
    num_iterations::Int = 1000,
    learning_rate::Int = 8,
    decay::Int = 100,
    tolerance::Float64 = 0.0,
    i_CP::Int = 1)

    # randomly initialize the key for starting optimization
    current_key::Tuple{Vararg{Int}} = rand(keys(all_params_preds_dict))
    metric_requirements::Int64 = 0

    # initialize the best key and metric value
    best_key::Tuple{Vararg{Int}} = current_key
    best_metric::Float64 = -Inf

    # initialize the previous metric value for convergence check
    prev_metric::Float64 = -Inf

    indices_columns = [Symbol(env) => Float64[] for env in envorder]
    all_results_df = DataFrame((indices_columns..., cp_value = Float64[]))
    successful_steps_df = DataFrame((indices_columns..., cp_value = Float64[]))

    tested_keys::Vector{Tuple{Vararg{Int}}} = []

    # perform optimization
    for iteration in 1:num_iterations
        if iteration % 100 == 0
            println("Iteration $iteration of $num_iterations")
            println("Best CP: $best_metric")
        end

        metric::Float64 = 0.0
        if metric_requirements == 0 # determine once which data is required by CP function
            try 
                if separate_columns == nothing
                    metric = constraint_potential_metric_func(all_params_preds_dict[current_key])[i_CP]
                    metric_requirements = 1
                else
                    metric = constraint_potential_metric_func(all_params_preds_dict[current_key], separate_columns)[i_CP]
                    metric_requirements = 2
                end
            catch
                if separate_columns == nothing
                    metric = constraint_potential_metric_func(all_params_preds_dict[current_key], all_params_dict[current_key])[i_CP]
                    metric_requirements = 3
                else
                    metric = constraint_potential_metric_func(all_params_preds_dict[current_key], all_params_dict[current_key], separate_columns)[i_CP]
                    metric_requirements = 4
                end
            end
        else # in further iterations, omit test
            if metric_requirements == 1
                metric = constraint_potential_metric_func(all_params_preds_dict[current_key])[i_CP]
            elseif metric_requirements == 2
                metric = constraint_potential_metric_func(all_params_preds_dict[current_key], separate_columns)[i_CP]
            elseif metric_requirements == 3
                metric = constraint_potential_metric_func(all_params_preds_dict[current_key], all_params_dict[current_key])[i_CP]
            else
                metric = constraint_potential_metric_func(all_params_preds_dict[current_key], all_params_dict[current_key], separate_columns)[i_CP]
            end
        end

        # update the best key and metric if necessary
        if metric > best_metric
            best_key = current_key
            best_metric = metric
            push!(successful_steps_df, ([all_params_dict[current_key][!,env][1] for env in envorder]..., metric))
            # for indices: push!(successful_steps_df, (collect(current_key)..., metric))
        end

        # update the learning rate based on the iteration
        if (iteration % decay == 0) && (learning_rate > 1)
            learning_rate -= 1  # Decay the learning rate
        end

        # check for convergence based on tolerance
        if abs(metric - prev_metric) < tolerance
            break
        end

        # update the previous metric for convergence check
        prev_metric = metric

        push!(all_results_df, ([all_params_dict[current_key][!,env][1] for env in envorder]..., metric))
        #for indices: push!(all_results_df, (collect(current_key)..., metric))

        # update the random key by randomly selecting a neighboring key
        stopat = 1000
        while stopat != 0
            new_key = select_random_neighbor(best_key, learning_rate)
            stopat = stopat-1
            if stopat == 0 # no ustested key found after 1000 attempts
                println("Stopped optimization because no untested key was encountered.")
                CSV.write(joinpath(output_directory,"optimization_results_allsteps.csv"), all_results_df)
                CSV.write(joinpath(output_directory,"optimization_results_successful_steps.csv"), successful_steps_df)
                return
            end
            if collect(new_key) ∈ tested_keys
                continue
            else
                try
                    all_params_preds_dict[new_key]
                    current_key = new_key
                    push!(tested_keys, current_key)
                    break
                catch
                    continue
                end
            end
        end

    end
    
    CSV.write(joinpath(output_directory,"optimization_results_allsteps.csv"), all_results_df)
    CSV.write(joinpath(output_directory,"optimization_results_successful_steps.csv"), successful_steps_df)

    return
end

function select_random_neighbor(key::Tuple{Vararg{Int}}, learning_rate::Int)::Tuple{Vararg{Int}}
    num_dimensions = length(key)
    neighbor = collect(key)

    # determine number of dimensions to modify
    num_dimensions_to_modify = rand(1:num_dimensions)

    # generate random dimensions to modify
    dimensions_to_modify = rand(1:num_dimensions, num_dimensions_to_modify)

    # modify selected dimensions
    for dimension in dimensions_to_modify
        neighbor[dimension] += rand(-learning_rate:learning_rate)
    end

    return tuple(neighbor...)
end

end # module