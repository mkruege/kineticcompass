# KINETIC COMPASS
# General framework for the Kinetic Compass for experiment design/prioritization
# Matteo Krüger (m.krueger@mpic.de)
# Multiphase Chemistry Department
# Max Planck Institute for Chemistry, Mainz
# Last update: 2023/09/27


module KineticCompass

import CSV
import DataFrames
import MAT
import Serialization
import Plots
import Combinatorics
import Random
import Dierckx
import Statistics
import MATLAB
import StatsPlots
include("KCModules.jl")


using CSV, DataFrames, MAT, Serialization, Plots, Combinatorics, Statistics, StatsPlots
using .KCModules

function run_kinetic_compass(output_directory::AbstractString, 
    fitfile::AbstractString, n_fits::Integer, 
    constraint_potential_metric_name::AbstractString,
    constraint_potential_metric_func::Function, 
    gridspec::Dict{String, Vector{Any}}, 
    set_params::Dict{String, Float64}, 
    envorder::Vector{String}; 
    CP_optimizer_func::Union{Function, Nothing} = nothing, 
    model_output_grid::Union{AbstractString, Nothing} = nothing, 
    output_model::Union{Function, Nothing} = nothing, 
    separate_fitfile_columns::Vector{String}=String[], 
    evaluate_full_input_grid::Bool = true, 
    verbose::Bool = true)

    println("Launching the kinetic compass method for the metric " * constraint_potential_metric_name * ".")

    # create output directory
    output_path::AbstractString = joinpath(pwd(), output_directory)
    if !isdir(output_path)
        mkdir(output_path)
    end

    # generate grid as specified in gridspec
    scales::Dict{String, Vector{Float64}} = get_grid(gridspec)
    if verbose
        println("Successfully generated grid of environmental parameters.")
    end

    # load fits from file and delete columns specified as separate
    fits::DataFrame = extract_fits(fitfile, n_fits, nothing)
    if size(separate_fitfile_columns,1) > 0
        separate_cols::Union{Vector{Vector{Any}}, Nothing} = []
        for sepcol in separate_fitfile_columns
            if sepcol in names(fits)
                fits = select(fits, Not(sepcol))
                current_col_vec = extract_fits(fitfile, n_fits, sepcol)
                if current_col_vec != nothing
                    push!(separate_cols, current_col_vec)
                end
            end
        end
    else
        separate_cols = nothing
    end
    if verbose
        println("Successfully loaded fit parameters.")
    end

    # read output structure from specified directory or file, or obtain predictive model
    if model_output_grid !== nothing # input grid overrules
        if endswith(model_output_grid, ".jls") # check if grid_file has been saved previously -> get in and outputs from file
            all_params::Vector{DataFrame}, all_params_preds::Vector{Vector{Vector{Float64}}} = deserialize(model_output_grid)
            if verbose
                println("Obtained model in and output grid from .jls file. If this is not intended, remove .jls file and specify the original grid data in 'output_grid_file', or assign a predictive model 'output_model'.")
            end
        else # only model outputs provided in file or directory
            # generate model inputs for grid by joining fits and mutations of env. parameters...
            all_params = generate_input_paramsets(scales, set_params, fits, envorder)
                if verbose
                    println("Successfully generated specified input grid data.")
                end
            #... and read model outputs from directory
            all_params, all_params_preds = transform_grid_mat_to_jls(model_output_grid, output_path, all_params, gridspec, envorder, n_fits)
            println("Obtained model output grid as a directory with .csv or .mat files and saved .jls file for later use.")
        end
        # transform data to dictionary of DataFrames (key is Tuple(index for each grid dimension))
        all_params_dict::Dict{Tuple{Vararg{Int}}, DataFrame} = transform_data(all_params, gridspec, envorder)
        all_params_preds_dict::Dict{Tuple{Vararg{Int}}, DataFrame} = transform_data(all_params_preds, gridspec, envorder)
        
    else # specified predictive model, no full grid
        if output_model !== nothing
            # generate model inputs for grid by joining fits and mutations of env. parameters...
            all_params = generate_input_paramsets(scales, set_params, fits, envorder)
            all_params_dict = transform_data(all_params, gridspec, envorder)
            if verbose
                println("Successfully generated specified input grid data.")
            end
            if evaluate_full_input_grid
                println("Warning: You are evaluating the full input grid using a predictive model. If your model is not a Julia model, it is recommended to evaluate the full grid within its original environment to ensure good performance. The full grid can be passed via 'model_output_grid'.")
                # use specified model to evaluate full grid of model outputs
                all_params_preds = output_model(all_params)
                all_params_preds_dict = transform_data(all_params_preds, gridspec, envorder)
                serialize(joinpath(output_path, "current_full_grid" * ".jls"), (all_params, all_params_preds))
                if verbose
                    println("Obtained all model predictions and saved full grid data as .jls file for later use.")
                end
            end
        else
            throw(ArgumentError("Either 'output_grid_file', or predictive model ('output_model') must be specified! Both arguments are 'nothing'."))
        end
    end

    # if constraint_of_interest is selected as metric, generate separate columns from output
    try
        global target_conditions
        if isnothing(separate_cols)
            println("No separate columns specified, but required for 'target constraint' metric. Generating outputs for desired condition as separate columns...")
            target_ensemble = get_target_ensemble(target_conditions, output_model, all_params_dict, all_params_preds_dict, gridspec)
            # translate to separate_columns
            separate_cols = Vector{Vector{Float64}}(undef, ncol(target_ensemble))
            for (i, col) in enumerate(eachcol(target_ensemble))
                separate_cols[i] = collect(col)
            end
        end
    catch
    end

    if evaluate_full_input_grid # full input grid is evaluated with regards to constraint potential
        start_time = time()
        if verbose
            println("Evaluating kinetic compass for full input grid. If this is not desired, set 'evaluate_full_input_grid' to false and make sure to specify an optimizer.")
        end
        all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}} = Dict{Tuple{Vararg{Int}}, Vector{Float64}}()
        try # test which input is accepted by the CP metric and evaluate full grid
            all_CP_dict = evaluate_full_grid(all_params_preds_dict, constraint_potential_metric_func, all_params_dict, separate_cols)
        catch
            all_CP_dict = evaluate_full_grid(all_params_preds_dict, constraint_potential_metric_func, nothing, separate_cols)
        end
        end_time = time()
        if verbose
            println("Runtime for grid evaluation: $(round((end_time - start_time),digits=3))s.")
        end
        if length(collect(values(all_CP_dict))[1]) == 1
            serialize(joinpath(output_path, "KC_evaluation_full_grid" * ".jls"), (all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, ["single"]))
        else
            serialize(joinpath(output_path, "KC_evaluation_full_grid" * ".jls"), (all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, names(fits)))
        end
        if verbose
            println("Saved results in 'KC_evaluation_full_grid.jls'. You can use this file for further analysis and plotting.")
        end
        sort_CP_dict(all_CP_dict, fits, scales, envorder, output_directory, gridspec)
        if length(keys(gridspec)) > 2
            println("The specified grid has more than two dimensions. Contourplots are all cuts of the hypersurface that contain the absolute constraint potential maximum.")
        end
        if length(collect(values(all_CP_dict))[1]) == 1 # plot for a single CP metric
            plot_contour(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, ["single"] , output_path)
            plot_barplot(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, ["single"], output_path)
            plot_heatmap(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, ["single"] , output_path)
        else # plot for one CP metric per kinetic parameter
            plot_contour(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, names(fits), output_path)
            plot_barplot(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, names(fits), output_path)
            plot_heatmap(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, names(fits), output_path)
            plot_multi_cp_comparison(all_CP_dict, scales, gridspec, envorder, constraint_potential_metric_name, names(fits), output_path)
        end
        if verbose
            println("Saved and plotted results from KC evaluation.")
        end
    elseif CP_optimizer_func !== nothing # if not evaluating full grid, launch optimizer to find CP maximum
        if verbose
            println("Starting optimizer to seach for constraint potential maximum in grid.")
        end
        if model_output_grid === nothing # if model output grid is not provided, call optimizer with model
            CP_optimizer_func(output_model, constraint_potential_metric_func, output_directory, all_params_dict, separate_cols, envorder)
        else # else call optimizer on fully evaluated grid
            println("Warning: You are running an optimization function on a fully evaluated ensemble solution grid. Usually, the KC evaluation of the full grid is computationally much cheaper than the model evaluation. Consider setting 'evaluate_full_input_grid' to true.")
            CP_optimizer_func(all_params_preds_dict, constraint_potential_metric_func, output_directory, all_params_dict, separate_cols, envorder)
        end
    else
        throw(ArgumentError("If 'evaluate_full_input_grid' is set to false, you need to provide a 'CP_optimizer_func'."))
    end

    return 0

end

function get_target_ensemble(target_conditions::Dict{String, Float64}, output_model::Union{Nothing, Function}, all_params_dict::Dict{Tuple{Vararg{Int}}, DataFrame}, all_params_preds_dict::Dict{Tuple{Vararg{Int}}, DataFrame}, gridspec::Dict{String, Vector{Any}})
    # check if an output model is provided
    if output_model !== nothing
        # use model to obtain target ensemble
        inputs = copy(collect(values(all_params_dict))[1])

        # Replace values in inputs with target_conditions for matching columns
        for param in keys(target_conditions)
            inputs[!, Symbol(param)] .= target_conditions[param]
        end

        # call the model to get target ensemble
        predictions = output_model([inputs])[1]
        column_names = Symbol.("out", 1:size(predictions[1], 1))
        matrix_data = hcat(predictions...)
        pred_df = DataFrame(transpose(matrix_data), column_names)
        println("Obtained target ensemble from model.")
        return pred_df

    else # obtain target ensemble from output grid

        # find the closest conditions in all_params_dict
        closest_key = find_closest_key(target_conditions::Dict{String, Float64}, all_params_dict::Dict{Tuple{Vararg{Int}}, DataFrame}, gridspec::Dict{String, Vector{Any}})
        
        # use the closest_key to access associated outputs in all_params_preds_dict
        if haskey(all_params_preds_dict, closest_key)
            println("Obtained target ensemble from grid using the closest grid point available:")
            for param in collect(keys(target_conditions))
                println(param)
                println(all_params_dict[closest_key][1, param])
            end
            return all_params_preds_dict[closest_key]
        else
            throw(ArgumentError("No associated predictions found for closest conditions"))
        end
    end
end

function find_closest_key(target_conditions::Dict{String, Float64}, all_params_dict::Dict{Tuple{Vararg{Int}}, DataFrame}, gridspec::Dict{String, Vector{Any}})
    min_distance = Inf
    closest_key = nothing

    for key in keys(all_params_dict)
        inputs = all_params_dict[key]

        # calculate the distance between target_conditions and input grid conditions
        distance = calculate_distance(target_conditions, inputs, gridspec)

        if distance < min_distance
            min_distance = distance
            closest_key = key
        end
    end

    return closest_key
end

function calculate_distance(target_conditions::Dict{String, Float64}, inputs::DataFrame, gridspec::Dict{String, Vector{Any}})
    # calculate the distance between target_conditions and input grid conditions
    distance = 0.0
    for param in keys(target_conditions)
        try
            if gridspec[param][3] == 0
                distance += (target_conditions[param] - inputs[1, Symbol(param)])^2
            else
                distance += (log10(target_conditions[param]) - log10(inputs[1, Symbol(param)]))^2
            end
        catch
        end
    end
    return sqrt(distance)
end

function get_grid(gridspec::Dict{String, Vector{Any}})::Dict{String, Vector{Float64}}
    # helper function to calculate the scale for a single dimension
    function scale(start::Float64, stop::Float64, size::Int, loggrid::Int)::Vector{Float64}
        if loggrid == 0
            if size == 1
                return [start]
            else
                return range(start, stop, length=size)
            end
        else
            return logrange(log10(start), log10(stop), size)
        end
    end
    
    # helper function to calculate log scale for a single dimension
    function logrange(start::Float64, stop::Float64, size::Int)::Vector{Float64}
        return exp10.(range(start, stop, length=size))
    end
    
    scales = Dict{String, Vector{Any}}()
    
    for (key, value) in gridspec
        bounds, size, loggrid = value
        lower_boundary, upper_boundary = bounds
        scale_size = size

        if length(bounds) != 2 # if more or less than 2 values are passed, it is assumed that these are the grid values
            scales[key] = bounds
        else # if there are 2 values, they must be boundaries
            scales[key] = scale(lower_boundary[1], upper_boundary[1], scale_size, loggrid)
        end
    end
    
    return scales
end

function extract_fits(fitfile::AbstractString, n_fits::Integer, separate_column::Union{Nothing, String})::Union{DataFrame, Vector{Int64}, Vector{Float64}, Vector{Any}}
    "Extract Fits from the provided file."
    allfits = CSV.read(fitfile, DataFrame)
    allfits = dropmissing(allfits)
    selectedfits = allfits[1:n_fits, :] # select top n_fits fits
    
    if separate_column != nothing
        try
            separate_column_vector = selectedfits[!, separate_column] # only return the separate column
            return separate_column_vector
        catch
            error("Couldn't find all columns in 'separate_fitfile_columns' in fit file. Confirm that they are present in 'fitfile'.")
        end
    end
    
    return selectedfits # pass all fits and delete separate column later
end

function generate_input_paramsets(scales::Dict{String, Vector{Float64}}, set_params::Dict{String, Float64}, fits::DataFrame, env_order::Vector{String})::Vector{DataFrame}
    # separate the mutable and set parameter names
    mutable_params = [param for param in env_order if param ∉ keys(set_params)]
    set_params_names = intersect(env_order, keys(set_params))
    
    # generate all combinations of mutable parameter values
    mutable_param_combinations = Iterators.product((scales[param] for param in mutable_params)...)
    all_preds_params = []

    for param_combination in mutable_param_combinations
        new_fits = copy(fits)
        
        # append associated values from env_order for mutable and set parameters
        env_params = DataFrame(zeros(size(fits, 1), size(env_order, 1)), :auto)
        rename!(env_params, env_order)
        mut_idx=1
        for param in env_order
            if param in mutable_params
                env_params[!, Symbol(param)] .= param_combination[mut_idx]
                mut_idx = mut_idx+1
            elseif param in set_params_names
                env_params[!, Symbol(param)] .= set_params[param]
            end
        end

        # concatenate ordered_fits with new_fits
        concatenated_fits = hcat(new_fits, env_params)
        
        # convert the DataFrame to the corresponding indices in the n-dimensional vector
        push!(all_preds_params, DataFrame(concatenated_fits))
    end
    
    return all_preds_params
end

function transform_grid_mat_to_jls(path::AbstractString, output_path::AbstractString, all_params::Vector{DataFrame}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, n_fits::Int)::Tuple{Vector{DataFrame}, Vector{Vector{Vector{Float64}}}}
    
    all_preds::Vector{Vector{Vector{Float64}}} = []

    # get individual files
    onlyfiles = filter(x -> endswith(x, ".mat"), readdir(path))

    if length(onlyfiles) == 0
        onlyfiles = filter(x -> endswith(x, ".csv"), readdir(path))

        if length(onlyfiles) == 0
            throw(ArgumentError("Unsupported pre-evaluated model output grid file type. Provide .mat or .csv files with model outputs."))
        end

        # collect data from CSV files
        all_data = []
        for (filenum, filename) in enumerate(onlyfiles)
            df = CSV.File(joinpath(path, filename); header=false) |> DataFrame
            data = Matrix(df)
            
            # check if cells contain a lists or a single values
            try
                data = [parse.(Float64, split(cell, ", ")) for cell in data]
            catch
                data = [[cell] for cell in data]
            end
            push!(all_data, data)
        end

        # transform data to grid structure
        mutable_params = [param for param in envorder if param ∈ keys(gridspec)]
        grid_shape = []
        for mutparam in mutable_params
            append!(grid_shape, gridspec[mutparam][2])
        end
        n_dims = length(grid_shape)

        indices = [collect(1:y) for y in grid_shape]
        comb = Iterators.product(indices...)
        index_combinations = collect(comb)
        for index_combination in index_combinations
            subgrid_arr = []
            for (fitnum, fit) in enumerate(all_data)
                if fitnum > n_fits
                    break
                end

                # handle different dimensions -> only up to 2 for csv
                if length(index_combination) == 1
                    x = index_combination[1]
                    push!(subgrid_arr, fit[x])
                elseif length(index_combination) == 2
                    x, y = index_combination
                    push!(subgrid_arr, fit[x, y])
                else
                    error("Unsupported number of dimensions for csv: $(length(index_combination))")
                end
            end
            push!(all_preds, subgrid_arr)
        end

        # save data structures
        serialize(joinpath(output_path, "current_full_grid" * ".jls"), (all_params, all_preds))
        return (all_params, all_preds)
    end

    # collect data from files
    all_data = []
    for (filenum, filename) in enumerate(onlyfiles)
        mat = matread(joinpath(path, filename))
        data = mat["GRID"]
        push!(all_data, data)
    end
    
    # transform data to grid structure
    mutable_params = [param for param in envorder if param ∈ keys(gridspec)]
    grid_shape = []
    for mutparam in mutable_params
        append!(grid_shape, gridspec[mutparam][2])
    end
    n_dims = length(grid_shape)
    
    indices = [collect(1:y) for y in grid_shape]
    comb = Iterators.product(indices...)
    index_combinations = collect(comb)
    for index_combination in index_combinations
        subgrid_arr = []
        for (fitnum, fit) in enumerate(all_data)
            if fitnum > n_fits
                break
            end
            if length(index_combination) == 1
                x = index_combination
                push!(subgrid_arr, fit[x, :])
            elseif length(index_combination) == 2
                x, y = index_combination
                push!(subgrid_arr, fit[x, y, :])
            elseif length(index_combination) == 3
                x, y, z = index_combination
                push!(subgrid_arr, fit[x, y, z, :])
            elseif length(index_combination) == 4
                x, y, z, a = index_combination
                push!(subgrid_arr, fit[x, y, z, a, :])
            elseif length(index_combination) == 5
                x, y, z, a, b = index_combination
                push!(subgrid_arr, fit[x, y, z, a, b, :])
            elseif length(index_combination) == 6
                x, y, z, a, b, c = index_combination
                push!(subgrid_arr, fit[x, y, z, a, b, c, :])
            elseif length(index_combination) == 7
                x, y, z, a, b, c, d = index_combination
                push!(subgrid_arr, fit[x, y, z, a, b, c, d, :])
            elseif length(index_combination) == 8
                x, y, z, a, b, c, d, e = index_combination
                push!(subgrid_arr, fit[x, y, z, a, b, c, d, e, :]) 
            else # only up to 8 dimensions...
                error("Unsupported number of dimensions: $(length(index_combination))")  
            end
        end
        push!(all_preds, subgrid_arr) 
    end

    # save both data structures
    serialize(joinpath(output_path, "current_full_grid" * ".jls"), (all_params, all_preds))
    return (all_params, all_preds)
end

function transform_data(fullgrid::Vector{Vector{Vector{Float64}}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String})::Dict{Tuple{Vararg{Int}}, DataFrame}
    mutable_params = [param for param in envorder if param ∈ keys(gridspec)]
    grid_shape = []
    for mutparam in mutable_params
        append!(grid_shape, gridspec[mutparam][2])
    end
    n_dims = length(grid_shape)

    all_params_preds_matrix = Dict{Tuple{Vararg{Int, n_dims}}, DataFrame}() # dict to save predictions

    # fill the dictionary with the data
    indices = [collect(1:y) for y in grid_shape]
    comb = Iterators.product(indices...)
    index_combinations = collect(comb)

    for (ind_n, index_combination) in enumerate(index_combinations)
        column_names = Symbol.("out", 1:size(fullgrid[ind_n][1], 1))
        df = DataFrame()
        for i in 1:length(fullgrid[ind_n])
            sub_df = DataFrame(transpose(fullgrid[ind_n][i]), column_names)
            df = vcat(df, sub_df)
        end
        # extract indices based on the number of dimensions
        if n_dims == 1
            x = index_combination[1]
            all_params_preds_matrix[(x,)] = df
        elseif n_dims == 2
            x, y = index_combination[1:2]
            all_params_preds_matrix[(x, y)] = df
        elseif n_dims == 3
            x, y, z = index_combination[1:3]
            all_params_preds_matrix[(x, y, z)] = df
        elseif n_dims == 4
            x, y, z, w = index_combination[1:4]
            all_params_preds_matrix[(x, y, z, w)] = df
        elseif n_dims == 5
            x, y, z, w, v = index_combination[1:5]
            all_params_preds_matrix[(x, y, z, w, v)] = df
        elseif n_dims == 6
            x, y, z, w, v, u = index_combination[1:6]
            all_params_preds_matrix[(x, y, z, w, v, u)] = df
        elseif n_dims == 7
            x, y, z, w, v, u, t = index_combination[1:7]
            all_params_preds_matrix[(x, y, z, w, v, u, t)] = df
        elseif n_dims == 8
            x, y, z, w, v, u, t, s = index_combination[1:8]
            all_params_preds_matrix[(x, y, z, w, v, u, t, s)] = df
        else
            error("Number of dimensions greater than 8 is not supported.")
        end
    end

    return all_params_preds_matrix
end

# overload function for DataFrames
function transform_data(fullgrid::Vector{DataFrame}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String})::Dict{Tuple{Vararg{Int}}, DataFrame}
    mutable_params = [param for param in envorder if param ∈ keys(gridspec)]
    grid_shape = []
    for mutparam in mutable_params
        append!(grid_shape, gridspec[mutparam][2])
    end
    n_dims = length(grid_shape)

    all_params_matrix = Dict{Tuple{Vararg{Int, n_dims}}, DataFrame}() # dict to save predictions

    # fill the dictionary with the data
    indices = [collect(1:y) for y in grid_shape]
    comb = Iterators.product(indices...)
    index_combinations = collect(comb)

    for (ind_n, index_combination) in enumerate(index_combinations)
        # extract indices based on the number of dimensions
        if n_dims == 1
            x = index_combination[1]
            all_params_matrix[(x,)] = fullgrid[ind_n]
        elseif n_dims == 2
            x, y = index_combination[1:2]
            all_params_matrix[(x, y)] = fullgrid[ind_n]
        elseif n_dims == 3
            x, y, z = index_combination[1:3]
            all_params_matrix[(x, y, z)] = fullgrid[ind_n]
        elseif n_dims == 4
            x, y, z, w = index_combination[1:4]
            all_params_matrix[(x, y, z, w)] = fullgrid[ind_n]
        elseif n_dims == 5
            x, y, z, w, v = index_combination[1:5]
            all_params_matrix[(x, y, z, w, v)] = fullgrid[ind_n]
        elseif n_dims == 6
            x, y, z, w, v, u = index_combination[1:6]
            all_params_matrix[(x, y, z, w, v, u)] = fullgrid[ind_n]
        elseif n_dims == 7
            x, y, z, w, v, u, t = index_combination[1:7]
            all_params_matrix[(x, y, z, w, v, u, t)] = fullgrid[ind_n]
        elseif n_dims == 8
            x, y, z, w, v, u, t, s = index_combination[1:8]
            all_params_matrix[(x, y, z, w, v, u, t, s)] = fullgrid[ind_n]
        else
            error("Number of dimensions greater than 8 is not supported.")
        end
    end

    return all_params_matrix
end

function evaluate_full_grid(all_params_preds_dict::Dict{Tuple{Vararg{Int}}, DataFrame}, constraint_potential_metric_func::Function, all_params_dict::Union{Dict{Tuple{Vararg{Int}}, DataFrame}, Nothing} = nothing, separate_columns::Union{Nothing, Vector{Vector{Any}}} = nothing)::Dict{Tuple{Vararg{Int}}, Vector{Float64}}
    evaluated_dict = Dict{Tuple{Vararg{Int}}, Vector{Float64}}()

    for (step, key) in enumerate(keys(all_params_preds_dict)) # get every key-value pair
        if step%1000 == 0 # display progress every 1000 steps
            println("Step $(step) of $(length(keys(all_params_preds_dict)))")
        end
        if all_params_dict == nothing # after top-level try/catch, correct arguments for metric are passed
            if separate_columns == nothing
                evaluated_dict[key] = constraint_potential_metric_func(all_params_preds_dict[key])
            else
                evaluated_dict[key] = constraint_potential_metric_func(all_params_preds_dict[key], separate_columns)
            end
        else
            if separate_columns == nothing
                evaluated_dict[key] = constraint_potential_metric_func(all_params_preds_dict[key], all_params_dict[key])
            else
                evaluated_dict[key] = constraint_potential_metric_func(all_params_preds_dict[key], all_params_dict[key], separate_columns)
            end
        end
    end

    return evaluated_dict
end

function sort_CP_dict(all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, fits::DataFrame, scales::Dict{String, Vector{Float64}}, envorder::Vector{String}, output_directory::AbstractString, gridspec::Dict{String, Vector{Any}})
    keys_values = collect(keys(all_CP_dict))
    colnames = []
    col_vals = []
    col_mutes = []

    for i in 1:(length(all_CP_dict[keys_values[1]]) == 1 ? 1 : length(all_CP_dict[keys_values[1]]) - length(envorder)) # omit environmental parameters if one value for each param is passed (parameter constraint potential)
        if length(all_CP_dict[keys_values[1]]) == 1
            push!(colnames, "CP")
        else
            push!(colnames, "CP_" * names(fits)[i])
        end

        # sort keys based on the values in the vector
        sort_indices = sortperm([all_CP_dict[key][i] for key in keys_values], rev=true)
        sorted_keys_values = keys_values[sort_indices]

        # sort associated keys (index)
        sorted_envs = sort(envorder, by=x -> findfirst(isequal(x), envorder))

        # iterate over the sorted keys and retrieve the corresponding values and scales
        values = []
        mut_params = []
        for key in sorted_keys_values
            push!(values, all_CP_dict[key][i])
            mut_param = []
            for (env_n, env) in enumerate(sorted_envs)
                try
                    push!(mut_param, scales[env][key[env_n]])
                catch
                end
            end
            push!(mut_params, mut_param)
        end
        push!(col_vals, values)
        push!(col_mutes, mut_params)

        save_sorted_data(output_directory, last(colnames), sorted_envs, sort_indices, values, mut_params, gridspec)
    end


    # if the grid has two dimensions, additionally create 2d - csv file
    dims = length(collect(keys(all_CP_dict))[1])
    if dims == 2
        for CP_i in 1:length(colnames)
            # determine correct size of result matrix
            size = []
            varenvs = []
            for param in envorder
                if haskey(gridspec, param)
                    append!(size, gridspec[param][2])
                    append!(varenvs, [param])
                end
            end
            # create matrix to collect results
            m = zeros(size[2], size[1])
            for (i, j) in collect(keys(all_CP_dict))
                m[j, i] = all_CP_dict[i, j][CP_i]
            end
            # transform to dataframe
            header = Symbol.(string.(scales[varenvs[1]]))
            index = scales[varenvs[2]]
            df = DataFrame(m, header)
            df[!, Symbol.(varenvs[2]*"/"*varenvs[1])] = index
            df = select(df, Symbol.(varenvs[2]*"/"*varenvs[1]), Not(Symbol.(varenvs[2]*"/"*varenvs[1])))
            output_path = joinpath(output_directory, "matrix_$(colnames[CP_i]).csv")
            CSV.write(output_path, df)
        end
    end
end

function save_sorted_data(output_directory::AbstractString, colname::AbstractString, sorted_envs::Vector{String}, sort_indices::Vector{Int64}, values::Vector{Any}, mut_params::Vector{Any}, gridspec::Dict{String, Vector{Any}})
    # create file path
    filepath = joinpath(output_directory, "sorted_$(colname).csv")

    # prepare data for saving
    data = Dict(
        "run_id" => sort_indices,
        colname => values
    )

    for (i, env) in enumerate(sorted_envs)
        if env in collect(keys(gridspec))
            data[env] = [value[i] for value in mut_params]
        end
    end

    # write the data to a CSV file
    CSV.write(filepath, data, header=collect(keys(data)), writeheader=true)
    println("Saved compass results at $(filepath)")
end

function plot_contour(all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String}, output_directory::AbstractString; cp_lims::Union{Tuple{Float64, Float64}, Nothing} = nothing, colmap::Symbol = :turbo, levels::Int = 20, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing)

    # make output directory if not present
    output_path::AbstractString = joinpath(@__DIR__, output_directory)
    if !isdir(output_path)
        mkdir(output_path)
    end

    # assemble env parameter list in correct order
    paramlist::Vector{String} = []
    for env in envorder
        if env in collect(keys(scales))
            push!(paramlist, env)
        end
    end

    dims = length(collect(keys(all_CP_dict))[1])

    n_subvals = length(collect(values(all_CP_dict))[1])
    if n_subvals != 1
        n_subvals = n_subvals - length(envorder)
    end
    for i_CP in 1:n_subvals
        # find the key corresponding to the maximum CP value
        max_value = maximum([x[i_CP] for x in collect(values(all_CP_dict))])
        max_key = nothing

        for (key, value) in all_CP_dict
            if value[i_CP] == max_value
                max_key = key
                break
            end
        end

        # create all possible combinations of two parameter indices
        param_combinations = combinations(1:length(paramlist), 2)

        for (param1_idx, param2_idx) in param_combinations # loop over all parameter combinations
            contour_vals = zeros(length(scales[paramlist[param2_idx]]), length(scales[paramlist[param1_idx]]))

            for key in collect(keys(all_CP_dict))  # loop over all data points in all_CP_dict
                # set the parameter indices to use for collecting data to the maximum key
                indices = Tuple([i in [param1_idx, param2_idx] ? key[i] : max_key[i] for i in 1:dims])

                # collect data
                contour_vals[key[param2_idx], key[param1_idx]] = all_CP_dict[indices][i_CP]
            end

            if n_subvals > 1
                title = CP_name * " " * CP_subnames[i_CP]
            else
                title = CP_name
            end

            # create the contour plot
            if isnothing(cp_lims)
                p = contourf(scales[paramlist[param1_idx]], scales[paramlist[param2_idx]], contour_vals; levels=levels, color=colmap, xlabel=paramlist[param1_idx], ylabel=paramlist[param2_idx], colorbar_title=title)
            else 
                p = contourf(scales[paramlist[param1_idx]], scales[paramlist[param2_idx]], contour_vals; levels=levels, color=colmap, xlabel=paramlist[param1_idx], ylabel=paramlist[param2_idx], colorbar_title=title, clim=cp_lims)
            end

            # plot previous experiments if provided
            if prev_experiments !== nothing
                for experiment in prev_experiments
                    x_vals = experiment[param1_idx]
                    y_vals = experiment[param2_idx]
                    scatter!(p, [x_vals], [y_vals], label="", c="white", ms=5)
                end
            end

            # log axis if grid is logarithmic
            if gridspec[paramlist[param1_idx]][3] == 1 # log axis
                contourf!(xaxis=:log10)
            end
            if gridspec[paramlist[param2_idx]][3] == 1
                contourf!(yaxis=:log10)
            end
            savefig(joinpath(output_path, "contour_plot_$(replace(CP_name, ' ' => '_'))_$(CP_subnames[i_CP])_$(paramlist[param1_idx])_$(paramlist[param2_idx]).pdf"))
        end

        # associate the indices with parameter values using envorder and scales
        max_param_values = Dict{String, Float64}()
        for (i, param_name) in enumerate(envorder)
            if param_name in keys(scales)
                param_value = scales[param_name][max_key[i]]
                max_param_values[param_name] = param_value
            end
        end

        # Print the environmental parameter values for absolute CP maximum
        println("Environmental parameter values for absolute $(CP_subnames[i_CP]) maximum:")
        for (param, value) in max_param_values
            println("$param: $(round(value, digits=3))")
        end

    end
end

function plot_heatmap(all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String}, output_directory::AbstractString; colmap::Symbol = :turbo, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing)

    # make output directory if not present
    output_path::AbstractString = joinpath(@__DIR__, output_directory)
    if !isdir(output_path)
        mkdir(output_path)
    end

    # assemble env parameter list in correct order
    paramlist::Vector{String} = []
    for env in envorder
        if env in collect(keys(scales))
            push!(paramlist, env)
        end
    end

    dims = length(collect(keys(all_CP_dict))[1])

    n_subvals = length(collect(values(all_CP_dict))[1])
    if n_subvals != 1
        n_subvals = n_subvals - length(envorder)
    end
    for i_CP in 1:n_subvals
        # find the key corresponding to the maximum CP value
        max_value = maximum([x[i_CP] for x in collect(values(all_CP_dict))])
        max_key = nothing

        for (key, value) in all_CP_dict
            if value[i_CP] == max_value
                max_key = key
                break
            end
        end

        # create all possible combinations of two parameter indices
        param_combinations = combinations(1:length(paramlist), 2)

        for (param1_idx, param2_idx) in param_combinations # loop over all parameter combinations
            heatmap_vals = zeros(length(scales[paramlist[param2_idx]]), length(scales[paramlist[param1_idx]]))

            for key in collect(keys(all_CP_dict))  # loop over all data points in all_CP_dict
                # set the parameter indices to use for collecting data to the maximum key
                indices = Tuple([i in [param1_idx, param2_idx] ? key[i] : max_key[i] for i in 1:dims])

                # collect data
                heatmap_vals[key[param2_idx], key[param1_idx]] = all_CP_dict[indices][i_CP]
            end

            if n_subvals > 1
                title = CP_name * " " * CP_subnames[i_CP]
            else
                title = CP_name
            end

            # Create the heatmap plot
            p = heatmap(scales[paramlist[param1_idx]], scales[paramlist[param2_idx]], heatmap_vals; color=colmap, xlabel=paramlist[param1_idx], ylabel=paramlist[param2_idx], colorbar_title=title)

            # plot previous experiments if provided
            if prev_experiments !== nothing
                for experiment in prev_experiments
                    x_vals = experiment[param1_idx]
                    y_vals = experiment[param2_idx]
                    scatter!(p, [x_vals], [y_vals], label="", c="white", ms=5)
                end
            end
            
            #try
            #    global target_conditions
            #    target_vals = []
            #    for param in paramlist
            #        push!(target_vals, target_conditions[param])
            #    end
            #    scatter!(p, [target_vals[param1_idx]], [target_vals[param2_idx]], label="", c="purple", ms=5)
            #catch
            #end

            # Check for log axes
            if gridspec[paramlist[param1_idx]][3] == 1
                xaxis!(p, :log10)
            end
            if gridspec[paramlist[param2_idx]][3] == 1
                yaxis!(p, :log10)
            end

            savefig(joinpath(output_path, "heatmap_$(replace(CP_name, ' ' => '_'))_$(CP_subnames[i_CP])_$(paramlist[param1_idx])_$(paramlist[param2_idx]).pdf"))
        end

        # associate the indices with parameter values using envorder and scales
        max_param_values = Dict{String, Float64}()
        for (i, param_name) in enumerate(envorder)
            if param_name in keys(scales)
                param_value = scales[param_name][max_key[i]]
                max_param_values[param_name] = param_value
            end
        end

        # Print the environmental parameter values for absolute CP maximum
        println("Environmental parameter values for absolute $(CP_subnames[i_CP]) maximum:")
        for (param, value) in max_param_values
            println("$param: $(round(value, digits=3))")
        end

    end
end

function plot_barplot(all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String}, output_directory::AbstractString; barcolor::Symbol = :blue, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing)
    
    # make output directory if not present
    output_path::AbstractString = joinpath(@__DIR__, output_directory)
    if !isdir(output_path)
        mkdir(output_path)
    end

    # assemble env parameter list in correct order
    paramlist::Vector{String} = []
    for env in envorder
        if env in collect(keys(scales))
            push!(paramlist, env)
        end
    end

    n_subvals = length(collect(values(all_CP_dict))[1])
    if n_subvals != 1
        n_subvals = n_subvals - length(envorder)
    end
    for i_CP in 1:n_subvals
        # find the key corresponding to the maximum CP value
        max_value = maximum([x[i_CP] for x in collect(values(all_CP_dict))])
        max_key = nothing

        for (key, value) in all_CP_dict
            if value[i_CP] == max_value
                max_key = key
                break
            end
        end

        # create bar plot for each environmental parameter in gridspec
        for (param_idx, param_name) in enumerate([param for param in envorder if param ∈ keys(gridspec)])
            param_values = scales[param_name]
            cp_values = [Vector{Any}() for _ in 1:length(param_values)]

            for cp_key in collect(keys(all_CP_dict))
                push!(cp_values[cp_key[param_idx]], all_CP_dict[cp_key][i_CP])
            end

            cp_means = [mean(cp_values[i]) for i in 1:length(cp_values)]
            cp_stds = [std(cp_values[i]) for i in 1:length(cp_values)]

            # create bar plot
            if gridspec[paramlist[param_idx]][3] == 1 # log axis
                p = scatter(param_values, cp_means, yerr=cp_stds, legend=false, color=barcolor, ylabel=CP_name, xlabel=param_name, xaxis=:log10)
            else
                p = scatter(param_values, cp_means, yerr=cp_stds, legend=false, color=barcolor, ylabel=CP_name, xlabel=param_name)
            end

            ylims!(p, (0, ylims(p)[2])) # set lower y limit to 0

            # plot previous experiments if provided
            if prev_experiments !== nothing
                for experiment in prev_experiments
                    vline!([experiment[param_idx]], color=:red, linestyle=:solid, label="")
                end
            end
            
            savefig(joinpath(output_path, "bar_plot_$(replace(CP_name, ' ' => '_'))_$(CP_subnames[i_CP])_$(param_name).pdf"))
        end
    end
end

function plot_multi_cp_comparison(all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String}, output_directory::AbstractString)

    # make output directory if not present
    output_path::AbstractString = joinpath(@__DIR__, output_directory)
    if !isdir(output_path)
        mkdir(output_path)
    end

    # assemble env parameter list in correct order
    paramlist::Vector{String} = []
    for env in envorder
        if env in collect(keys(scales))
            push!(paramlist, env)
        end
    end

    n_subvals = length(collect(values(all_CP_dict))[1])
    if n_subvals != 1
        n_subvals = n_subvals - length(envorder)
    end
    
    param_names = keys(gridspec)

    cp_values = Matrix{Float64}(undef, length(collect(keys(all_CP_dict))), length(CP_subnames))
    
    # create boxplot for each environmental parameter in gridspec
    for (CP_i, CP_subname) in enumerate(CP_subnames)
        cp_values_param = Float64[]
        for cp_key in collect(keys(all_CP_dict))
            push!(cp_values_param, all_CP_dict[cp_key][CP_i])
        end
        cp_values[:, CP_i] = cp_values_param
    end

    xtick_positions = 1:length(CP_subnames)
    xtick_labels = CP_subnames

    xtick_positions_h = hcat(xtick_positions...)

    # create boxplot
    p = violin(xtick_positions_h, cp_values, legend=false, ylabel=CP_name, xlabel="Parameter", color=:blue, xrotation=45, xtickfontsize=8, xticks=(xtick_positions, xtick_labels))
    
    savefig(joinpath(output_path, "violin_plot_$(replace(CP_name, ' ' => '_')).pdf"))
end

#overload functions to handle input file for advanced plotting
function plot_contour(KC_output_file::AbstractString, output_directory::AbstractString; cp_lims::Union{Tuple{Float64, Float64}, Nothing} = nothing, colmap::Symbol = :turbo, levels::Int = 20, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing) 
    all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String} = deserialize(KC_output_file)
    plot_contour(all_CP_dict, scales, gridspec, envorder, CP_name, CP_subnames, output_directory; cp_lims=cp_lims, colmap=colmap, levels=levels, prev_experiments=prev_experiments)
    return
end

function plot_heatmap(KC_output_file::AbstractString, output_directory::AbstractString; colmap::Symbol = :turbo, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing) 
    all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String} = deserialize(KC_output_file)
    plot_heatmap(all_CP_dict, scales, gridspec, envorder, CP_name, CP_subnames, output_directory; colmap=colmap, prev_experiments=prev_experiments)
    return
end

function plot_barplot(KC_output_file::AbstractString, output_directory::AbstractString; barcolor::Symbol = :blue, prev_experiments::Union{Vector{Vector{Float64}}, Nothing} = nothing)
    all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String} = deserialize(KC_output_file)
    plot_barplot(all_CP_dict, scales, gridspec, envorder, CP_name, CP_subnames, output_directory; barcolor=barcolor, prev_experiments=prev_experiments)
    return
end

function plot_multi_cp_comparison(KC_output_file::AbstractString, output_directory::AbstractString)
    all_CP_dict::Dict{Tuple{Vararg{Int}}, Vector{Float64}}, scales::Dict{String, Vector{Float64}}, gridspec::Dict{String, Vector{Any}}, envorder::Vector{String}, CP_name::String, CP_subnames::Vector{String} = deserialize(KC_output_file)
    plot_multi_cp_comparison(all_CP_dict, scales, gridspec, envorder, CP_name, CP_subnames, output_directory)
    return
end

function set_target(val::Dict{String, Float64})
    global target_conditions = val
end

end # module